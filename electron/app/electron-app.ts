import { BrowserWindow, ipcMain } from "electron";
import { injectable } from "inversify";
import * as path from "path";
import * as url from "url";

import { CustomerService } from "../server/service/customer.service";

@injectable()
export class ElectronApp {
  private mainWindow: BrowserWindow = null;

  constructor(private customerService: CustomerService) { }

  windowsExists() {
    return this.mainWindow === null;
  }

  async createMainWindow() {
    try {
      this.mainWindow = new BrowserWindow({
        width: 800,
        height: 600,
        backgroundColor: "#ffffff"
      });

      this.mainWindow.loadURL(
        url.format({
          pathname: path.join(__dirname, "../../angular/index.html"),
          protocol: "file:",
          slashes: true
        })
      );

      // Uncomment the line below to open DevTools
      this.mainWindow.webContents.openDevTools();

      this.mainWindow.on("closed", () => {
        this.mainWindow = null;
      });

      ipcMain.on("get-customers", async (event: any, ...args: any[]) => {
        try {
          event.returnValue = await this.customerService.get();
        } catch (err) {
          console.log(err);
        }
      });
    } catch (err) {
      throw err;
    }
  }
}
