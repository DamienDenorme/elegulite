import { app } from "electron";

import { ElectronApp } from "./app/electron-app";
import { container } from "./config/inversify.config";
import { TYPES } from "./config/types";
import { Database } from "./server/database";

const electronApp = container.get<ElectronApp>(TYPES.ElectronApp);
const database = container.get<Database>(TYPES.Database);

app.on("ready", async () => {
  try {
    await database.init();
    electronApp.createMainWindow();
  } catch (err) {
    console.log(err);
  }
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (!electronApp.windowsExists()) {
    electronApp.createMainWindow();
  }
});
