import { Container } from "inversify";

import { TYPES } from "./types";
import { ElectronApp } from "../app/electron-app";
import { Database } from "../server/database";
import { CustomerService } from "../server/service/customer.service";

const container = new Container({ defaultScope: "Singleton", autoBindInjectable: true });

container.bind<CustomerService>(TYPES.CustomerService).to(CustomerService);
container.bind<Database>(TYPES.Database).to(Database);
container.bind<ElectronApp>(TYPES.ElectronApp).to(ElectronApp);

export { container };
