export const TYPES = {
  CustomerService: Symbol.for("CustomerService"),
  Database: Symbol.for("Database"),
  ElectronApp: Symbol.for("ElectronApp")
};
