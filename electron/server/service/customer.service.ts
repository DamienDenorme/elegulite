import { injectable, inject } from "inversify";
import { Repository } from "typeorm";

import { TYPES } from "../../config/types";
import { Database } from "../database";
import { Customer } from "../entity/customer.entity";

@injectable()
export class CustomerService {
  @inject(TYPES.Database) private database: Database;

  private get repository(): Repository<Customer> {
    return this.database.getRepository(Customer);
  }

  constructor() { }

  async get() {
    return await this.repository.find();
  }
}
