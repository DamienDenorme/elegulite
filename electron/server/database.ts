import { injectable } from "inversify";
import { createConnection, Connection, Repository, ObjectType } from "typeorm";

import { Customer } from "./entity/customer.entity";

@injectable()
export class Database {
  private connection: Connection;

  constructor() { }

  async init() {
    this.connection = await this.connect();
  }

  private connect(): Promise<Connection> {
    return createConnection({
      type: "sqlite",
      synchronize: true,
      logging: true,
      logger: "simple-console",
      database: "./sqlite/database.sqlite",
      entities: [Customer]
    });
  }

  public getRepository<T>(entity: ObjectType<T>): Repository<T> {
    return this.connection.getRepository<T>(entity);
  }
}
