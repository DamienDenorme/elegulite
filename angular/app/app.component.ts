import { Component, OnInit } from "@angular/core";
import { CustomerService } from "./core/customer/customer.service";

import { Customer } from "./core/customer/customer.model";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  title = "elegulite";
  customers: Customer[];

  constructor(private customerService: CustomerService) {}

  ngOnInit() {
    try {
      this.customerService.getCustomers().subscribe(customers => (this.customers = customers));
    } catch (err) {
      console.log(err);
    }
  }
}
