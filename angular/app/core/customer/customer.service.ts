import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { Customer } from "electron/server/entity/customer.entity";

declare const electron: any;

@Injectable({
  providedIn: "root"
})
export class CustomerService {
  constructor() {}

  addCustomer(item: Customer): Observable<Customer[]> {
    return of(electron.ipcRenderer.sendSync("add-customer", item)).pipe(catchError((error: any) => Observable.throw(error.json)));
  }

  getCustomers(): Observable<any> {
    return of(electron.ipcRenderer.sendSync("get-customers")).pipe(catchError((error: any) => Observable.throw(error.json)));
  }

  updateCustomer(item: Customer): Observable<Customer[]> {
    return of(electron.ipcRenderer.sendSync("update-customer", item)).pipe(catchError((error: any) => Observable.throw(error.json)));
  }

  deleteCustomers(item: Customer): Observable<Customer[]> {
    return of(electron.ipcRenderer.sendSync("delete-customer", item)).pipe(catchError((error: any) => Observable.throw(error.json)));
  }
}
