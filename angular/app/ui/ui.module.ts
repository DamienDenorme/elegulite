import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormlyModule } from "@ngx-formly/core";
import { FormlyMaterialModule } from "@ngx-formly/material";

@NgModule({
  declarations: [],
  imports: [CommonModule, FormlyModule.forRoot(), FormlyMaterialModule]
})
export class UiModule {}
