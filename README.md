# Elegulite

Elegulite is an Electron boilerplate meant to be used for any kind of desktop application requiring data handling and saving.
It comprises Electron, Angular, Material, NgxFormly, Inversify and SQLite.

# Documentation

Go the [project wiki](https://gitlab.com/DamienDenorme/elegulite/wikis/home).

# Roadmap

- Add explanation diagrams in the wiki.
- Describe the architecture in the wiki.
- Confirm testing environment.
- Add interfaces for the services.
